/**
 * @brief スタック(long long専用)
 * 
 * @details 最大の要素の個数200000個
 */
class stack_lld
{
private:
    /**
     * @brief 本体の配列
     */
    long long n[200003];
    /**
     * @brief 値が入っている最後のインデックス
     */
    int index = -1;
public:
    /**
     * @brief 値の追加
     * 
     * @param arg 追加する値
     */
    void push(long long arg)
    {
        index++;
        n[index] = arg;
    }
    /**
     * @brief 値の削除
     * 
     * @return long long 削除した値 
     */
    long long pop()
    {
        if (index == -1)
        {
            return -1;
        }
        long long t = n[index];
        n[index] = -1;
        index--;
        return t;
    }
    /**
     * @brief 空かどうかの確認
     * 
     * @return true 空
     * @return false 空でない
     */
    bool isEmpty()
    {
        return index==-1;
    }
    /**
     * @brief 満杯かどうかの確認
     * 
     * @return true 満杯
     * @return false 満杯でない
     */
    bool isFull()
    {
        return index==200003;
    }
    /**
     * @brief 現在のサイズ
     * 
     * @return long long サイズ
     */
    long long size()
    {
        return index+1;
    }
};
