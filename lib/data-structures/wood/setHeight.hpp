#include <bits/stdc++.h>
using namespace std;
//二分岐構造の高さ
//O(N)
long long setHeight(long long H[], long long u, vector<tuple<long long, long long, long long>> Node)
{
    long long h1 = 0, h2 = 0;
    if (get<2>(Node[u]) != -1) h1 = setHeight(H, get<2>(Node[u]), Node) + 1;
    if (get<1>(Node[u]) != -1) h2 = setHeight(H, get<1>(Node[u]), Node) + 1;
    return H[u] = max(h1, h2);
}
