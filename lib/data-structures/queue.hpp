/**
 * @brief キュー(long long専用)
 * 
 * @details 最大の要素の個数100000個
 */
class queue_lld
{
private:
    /**
     * @brief 本体の配列
     */
    long long q[100003];
    /**
     * @brief 先頭の要素のインデックス
     */
    long long head;
    /**
     * @brief 末尾の要素のインデックス
     */
    long long tail;
public:
    /**
     * @brief コンストラクタ
     */
    queue_lld()
    {
        head = 0;
        tail = 0;
    }
    /**
     * @brief キューに挿入
     * 
     * @param x 挿入する値
     */
    void enqueue(long long x)
    {
        q[tail] = x;
        if (tail+1 == 100003)
        {
            tail = 0;
        }
        else
        {
            tail++;
        }
    }
    /**
     * @brief 先頭の要素を削除
     * 
     * @return long long 削除した値 
     */
    long long dequeue()
    {
        if (isEmpty())
        {
            return;
        }
        long long x = q[head];
        if (head + 1 == 100003)
        {
            head = 0;
        }
        else
        {
            head++;
        }
        return x;
    }
    /**
     * @brief 空かどうかの確認
     * 
     * @return true 空
     * @return false 空でない
     */
    bool isEmpty()
    {
        return head = tail;
    }
    /**
     * @brief 満杯かどうかの確認
     * 
     * @return true 満杯
     * @return false 満杯でない
     */
    bool isFull()
    {
        return head == (tail + 1) % 100003;
    }
};
