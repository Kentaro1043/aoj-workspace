/**
 * @brief 挿入ソート
 * 
 * @param a ソートする配列
 * @param n 配列のサイズ
 * @details O(n^2)、安定なソート
 */
void insertionSort(long long a[], long long n)
{
    for (int i = 1; i <= n - 1; i++)
    {
        long long v = a[i];
        long long j = i - 1;
        while(j >= 0 && a[j] > v)
        {
            a[j+1] = a[j];
            j--;
        }
        a[j+1] = v;
    }
}
