#include <bits/stdc++.h>
using namespace std;
//係数ソート
//O(n+k) 安定
void countingSort(long long a[], long long b[], long long n)
{
    long long k = *max_element(a, a+n);
    long long c[k+1];
    for (long long i = 0; i <= k; i++) c[i] = 0;
    for (long long j = 0; j < n; j++) c[a[j]]++;
    for (long long i = 1; i <= k; i++) c[i]+=c[i-1];
    for (long long j = n-1; j >= 0; j--)
    {
        b[c[a[j]]-1] = a[j];
        c[a[j]]--;
    }
}