int partition(int A[], int p, int r)
{
    int x = A[r];
    int i = p-1;
    for (int j = p; j < r; j++)
    {
        if (A[j] <= x)
        {
            i++;
            int t = A[i];
            A[i] = A[j];
            A[j] = t;
        }
    }
    int t = A[r];
    A[r] = A[i+1];
    A[i+1] = t;
    return i+1;
}
/**
 * @brief クイックソート
 * 
 * @param A ソートする配列
 * @param p ソート開始位置(先頭なら0)
 * @param r ソート終了位置(末尾なら個数-1)
 * @details O(nlogn),不安定なソート
 */
void quickSort(int A[], int p, int r)
{
    if (p < r)
    {
        int q = partition(A, p, r);
        quickSort(A, p, q-1);
        quickSort(A, q+1, r);
    }
}
