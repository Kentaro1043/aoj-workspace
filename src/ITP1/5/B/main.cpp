#include <iostream>
#include <vector>
using namespace std;
int main()
{
    vector<int> h, w;
    int count = 0;
    int t1, t2;
    while(true)
    {
        cin >> t1 >> t2;
        if (t1 == 0 && t2 == 0)
        {
            break;
        }
        h.push_back(t1);
        w.push_back(t2);
        count++;
    }
    for (int i = 0; i < count; i++)
    {
        for (int j = 0; j < h[i]; j++)
        {
            for (int k = 0; k < w[i]; k++)
            {
                if (j == 0 || j + 1 == h[i] || k == 0 || k + 1 == w[i])
                {
                    cout << "#";
                }
                else
                {
                    cout << ".";
                }
            }
            cout << endl;
        }
        cout << endl;
    }
    return 0;
}
