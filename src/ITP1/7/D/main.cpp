#include <iostream>
using namespace std;
int main()
{
    int n, m, l;
    cin >> n >> m >> l;
    long long a[n][m], b[m][l];
    for (int i = 0; i < n; i++)
    {
        for (int j = 0; j < m; j++)
        {
            cin >> a[i][j];
        }
    }
    for (int i = 0; i < m; i++)
    {
        for (int j = 0; j < l; j++)
        {
            cin >> b[i][j];
        }
    }
    long long c[n][l];
    for (int i = 0; i < n; i++)
    {
        for (int j = 0; j < l; j++)
        {
            long long t = 0;
            for (int k = 0; k < m; k++)
            {
                t += a[i][k] * b[k][j];
            }
            c[i][j] = t;
        }
    }
    for (int i = 0; i < n; i++)
    {
        for (int j = 0; j < l; j++)
        {
            cout << c[i][j];
            if (j != l - 1)
            {
                cout << " ";
            }
        }
        cout << endl;
    }
    return 0;
}
