#include <iostream>
#include <vector>
using namespace std;
int main()
{
    vector<int> m, f, r;
    int t1, t2, t3, count = 0;
    while(true)
    {
        cin >> t1 >> t2 >> t3;
        if (t1 == -1 && t2 == -1 && t3 == -1)
        {
            break;
        }
        m.push_back(t1);
        f.push_back(t2);
        r.push_back(t3);
        count++;
    }
    for (int i = 0; i < count; i++)
    {
        if (m[i] == -1 || f[i] == -1)
        {
            cout << "F" << endl;
        }
        else if (m[i] + f[i] >= 80)
        {
            cout << "A" << endl;
        }
        else if (m[i] + f[i] >= 65)
        {
            cout << "B" << endl;
        }
        else if (m[i] + f[i] >= 50)
        {
            cout << "C" << endl;
        }
        else if (m[i] + f[i] >= 30 || r[i] >= 50)
        {
            cout << "D" << endl;
        }
        else
        {
            cout << "F" << endl;
        }
    }
    return 0;
}
