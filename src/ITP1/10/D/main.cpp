#include <iostream>
#include <iomanip>
#include <cmath>
using namespace std;
int main()
{
    int n;
    cin >> n;
    int x[n], y[n];
    for (int i = 0; i < n; i++)
    {
        cin >> x[i];
    }
    for (int i = 0; i < n; i++)
    {
        cin >> y[i];
    }

    double m = 0;
    for (int i = 0; i < n; i++)
    {
        m += abs(x[i] - y[i]);
    }

    double u = 0;
    for (int i = 0; i < n; i++)
    {
        u += pow(abs(x[i] - y[i]), 2);
    }
    u = sqrt(u);

    double n3 = 0;
    for (int i = 0; i < n; i++)
    {
        n3 += pow(abs(x[i] - y[i]), 3);
    }
    n3 = pow(n3, 1.0 / 3);

    double t = 0, tmp = abs(x[0] - y[0]);
    for (int i = 0; i < n; i++)
    {
        tmp = abs(x[i] - y[i]);
        if (tmp > t)
        {
            t = tmp;
        }
    }

    cout << fixed << setprecision(6) << m << endl << u << endl << n3 << endl << t << endl;
}
