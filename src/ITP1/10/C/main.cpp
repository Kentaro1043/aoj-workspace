#include <iostream>
#include <iomanip>
#include <numeric>
#include <math.h>
using namespace std;
int main()
{
    while(true)
    {
        int n;
        cin >> n;
        if (n == 0)
        {
            break;
        }
        int s[n];
        for (int i = 0; i < n; i++)
        {
            cin >> s[i];
        }
        int m = accumulate(s, s+n, 0) / n;
        int t = 0;
        for (int i = 1; i <= n; i++)
        {
            t += pow(s[i] - m, 2);
        }
        t /= 2;
        cout << fixed << setprecision(10) << sqrt(t/n) << endl;
    }
    return 0;
}
