#include <iostream>
#include <vector>
using namespace std;
int main()
{
    vector<int> v;
    int tmp;
    while(true)
    {
        cin >> tmp;
        if (tmp == 0)
        {
            break;
        }
        else
        {
            v.push_back(tmp);
        }
    }
    for (int i = 1; i <= v.size(); i++)
    {
        cout << "case " << i << ": " << v[i] << endl;
    }
    return 0;
}
