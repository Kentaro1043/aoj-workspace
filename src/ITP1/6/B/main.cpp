#include <iostream>
#include <string>
#include <map>
using namespace std;
int main()
{
    int n;
    cin >> n;
    map<int, bool> s, h, c, d;
    for (int i = 0; i < 13; i++)
    {
        s[i] = false;
    }
    for (int i = 0; i < 13; i++)
    {
        h[i] = false;
    }
    for (int i = 0; i < 13; i++)
    {
        c[i] = false;
    }
    for (int i = 0; i < 13; i++)
    {
        d[i] = false;
    }
    string t1; int t2; 
    for (int i = 0; i < n; i++)
    {
        cin >> t1 >> t2;
        if (t1 == "S")
        {
            s[t2] = true;
        }
        if (t1 == "H")
        {
            h[t2] = true;
        }
        if (t1 == "C")
        {
            c[t2] = true;
        }
        if (t1 == "D")
        {
            d[t2] = true;
        }
    }
    for (int i = 1; i <= 13; i++)
    {
        if (!(s[i]))
        {
            cout << "S " << i << endl;
        }
    }
    for (int i = 1; i <= 13; i++)
    {
        if (!(h[i]))
        {
            cout << "H " << i << endl;
        }
    }
    for (int i = 1; i <= 13; i++)
    {
        if (!(c[i]))
        {
            cout << "C " << i << endl;
        }
    }
    for (int i = 1; i <= 13; i++)
    {
        if (!(d[i]))
        {
            cout << "D " << i << endl;
        }
    }
    return 0;
}
