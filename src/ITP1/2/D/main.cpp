#include <iostream>
using namespace std;
int main()
{
    int w,h,x,y,r;
    cin >> w >> h >> x >> y >> r;
    int a,b;
    a = x + r;
    if (!(a >=0 && a <= w))
    {
        cout << "No" << endl;
        return 0;
    }
    a = x - r;
    if (!(a >= 0 && a <= w))
    {
        cout << "No" << endl;
        return 0;
    }
    b = y + r;
    if (!(b >= 0 && b <= h))
    {
        cout << "No" << endl;
        return 0;
    }
    b = y - r;
    if (!(b >= 0 && b <= h))
    {
        cout << "No" << endl;
        return 0;
    }
    cout << "Yes" << endl;
    return 0;
}
