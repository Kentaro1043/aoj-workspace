#include <iostream>
#include <vector>
using namespace std;

class Dice
{
    private:
        int a=1, b=2, c=3, d=4, e=5,f=6;
    public:
        Dice(int n, int o, int p, int q, int r, int s)
        {
            a = n;
            b = o;
            c = p;
            d = q;
            e = r;
            f = s;
        }
        int getNumber(string arg)
        {
            int t;
            if (arg == "a")
            {
                t = a;
            }
            else if (arg == "b")
            {
                t = b;
            }
            else if (arg == "c")
            {
                t = c;
            }
            else if (arg == "d")
            {
                t = d;
            }
            else if (arg == "e")
            {
                t = e;
            }
            else if (arg == "f")
            {
                t = f;
            }
            return t;
        }
        void turn(string arg)
        {
            int beforeA = a, beforeB = b, beforeC = c, beforeD = d, beforeE = e, beforeF = f;
            if (arg == "N")
            {
                a = beforeB;
                b = beforeF;
                f = beforeE;
                e = beforeA;
            }
            else if (arg == "W")
            {
                a = beforeC;
                c = beforeF;
                f = beforeD;
                d = beforeA;
            }
            else if (arg == "E")
            {
                a = beforeD;
                c = beforeA;
                f = beforeC;
                d = beforeF;
            }
            else if (arg == "S")
            {
                a = beforeE;
                b = beforeA;
                f = beforeB;
                e = beforeF;
            }
        }
    string searchNumber(int n)
    {
        if (n == a)
        {
            return "a";
        }
        else if (n == b)
        {
            return "b";
        }
        else if (n == c)
        {
            return "c";
        }
        else if (n == d)
        {
            return "d";
        }
        else if (n ==e)
        {
            return "e";
        }
        else
        {
            return "f";
        }
    }
};

int main()
{
    int n, o, p, q, r, s;
    cin >> n >> o >> p >> q >> r >> s;
    Dice dice(n, o, p, q, r, s);
    cin >> q;
    for (int i = 0; i < q; i++)
    {
        int t1, t2;
        cin >> t1 >> t2;
        string arg1 = dice.searchNumber(t1);
        string arg2 = dice.searchNumber(t2);
        if (arg1 == "a" && arg2 == "b")
        {
            cout << dice.getNumber("c") << endl;
        }
        else if (arg1 == "a" && arg2 == "c")
        {
            cout << dice.getNumber("e") << endl;
        }
        else if (arg1 == "a" && arg2 == "d")
        {
            cout << dice.getNumber("b") << endl;
        }
        else if (arg1 == "a" && arg2 == "e")
        {
            cout << dice.getNumber("d") << endl;
        }
        else if (arg1 == "b" && arg2 == "a")
        {
            cout << dice.getNumber("d") << endl;
        }
        else if (arg1 == "b" && arg2 == "c")
        {
            cout << dice.getNumber("a") << endl;
        }
        else if (arg1 == "b" && arg2 == "d")
        {
            cout << dice.getNumber("f") << endl;
        }
        else if (arg1 == "b" && arg2 == "e")
        {
            cout << dice.getNumber("a") << endl;
        }
        else if (arg1 == "b" && arg2 == "f")
        {
            cout << dice.getNumber("c") << endl;
        }
        else if (arg1 == "c" && arg2 == "a")
        {
            cout << dice.getNumber("b") << endl;
        }
        else if (arg1 == "c" && arg2 == "b")
        {
            cout << dice.getNumber("f") << endl;
        }
        else if (arg1 == "c" && arg2 == "e")
        {
            cout << dice.getNumber("a") << endl;
        }
        else if (arg1 == "c" && arg2 == "f")
        {
            cout << dice.getNumber("e") << endl;
        }
        else if (arg1 == "d" && arg2 == "a")
        {
            cout << dice.getNumber("e") << endl;
        }
        else if (arg1 == "d" && arg2 == "b")
        {
            cout << dice.getNumber("a") << endl;
        }
        else if (arg1 == "d" && arg2 == "e")
        {
            cout << dice.getNumber("f") << endl;
        }
        else if (arg1 == "d" && arg2 == "f")
        {
            cout << dice.getNumber("b") << endl;
        }
        else if (arg1 == "e" && arg2 == "a")
        {
            cout << dice.getNumber("c") << endl;
        }
        else if (arg1 == "e" && arg2 == "c")
        {
            cout << dice.getNumber("f") << endl;
        }
        else if (arg1 == "e" && arg2 == "d")
        {
            cout << dice.getNumber("a") << endl;
        }
        else if (arg1 == "e" && arg2 == "f")
        {
            cout << dice.getNumber("d") << endl;
        }
        else if (arg1 == "f" && arg2 == "b")
        {
            cout << dice.getNumber("d") << endl;
        }
        else if (arg1 == "f" && arg2 == "c")
        {
            cout << dice.getNumber("b") << endl;
        }
        else if (arg1 == "f" && arg2 == "d")
        {
            cout << dice.getNumber("e") << endl;
        }
        else if (arg1 == "f" && arg2 == "e")
        {
            cout << dice.getNumber("c") << endl;
        }
    }
    return 0;
}
