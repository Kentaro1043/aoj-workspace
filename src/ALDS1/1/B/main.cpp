#include <bits/stdc++.h>
#define rep(i, n) for (int i = 0; i < (n); ++i)
#define all(x) (x).begin(), (x).end()
using namespace std;
typedef long long ll;
long long gcd_lld(long long x, long long y)
{
    if (x < y)
    {
        long long t = x;
        x = y;
        y = t;
    }
    while (y > 0)
    {
        long long r = x % y;
        x = y;
        y = r;
    }
    return x;
}
int main()
{
    ll x, y;
    scanf("%lld%lld", &x, &y);
    ll ans = gcd_lld(x, y);
    cout << ans << endl;
    return 0;
}
