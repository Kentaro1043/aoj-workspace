#include <bits/stdc++.h>
typedef long long ll;
#define rep(i, n) for (int i = 0; i < (n); i++)
using namespace std;
struct card {
    int value;
    char word;
};
bool isStable(card a[], card b[], int n)
{
    rep(i, n)
    {
        if (a[i].value != b[i].value || a[i].word != b[i].word)
        {
            return false;
        }
    }
    return true;
}
/**
 * @brief バブルソート
 * 
 * @param a ソートする配列
 * @param n 配列のサイズ
 * @details O(n^2)、安定なソート
 */
void bubbleSort(card a[], int n)
{
    long long flag = 1;
    while (flag)
    {
        flag = 0;
        long long i = 0;
        for (long long j = n - 1; j >= i+1; j--)
        {
            if (a[j].value < a[j-1].value)
            {
                swap(a[j], a[j-1]);
                flag = 1;
            }
        }
        i++;
    }
}
/**
 * @brief 選択ソート
 * 
 * @param a ソートする配列
 * @param n 配列のサイズ
 * @details O(n^2)、不安定なソート
 */
void selectionSort(card a[], long long n)
{
    for (long long i = 0; i < n; i++)
    {
        long long minj = i;
        for (long long j = i; j < n; j++)
        {
            if (a[j].value < a[minj].value)
            {
                minj = j;
            }
        }
        swap(a[i], a[minj]);
    }
}
int main()
{
    int n; cin >> n;
    card a[n], b[n];
    rep(i, n)
    {
        string s; cin >> s;
        a[i].value = s[1]-'0'; a[i].word = s[0];
        b[i].value = s[1]-'0'; b[i].word = s[0];
    }
    bubbleSort(a, n);
    selectionSort(b, n);
    rep(i, n)
    {
        cout << a[i].word << a[i].value;
        if (i != n-1) cout << " ";
    }
    cout << endl << "Stable" << endl;
    rep(i, n)
    {
        cout << b[i].word << b[i].value;
        if (i != n-1) cout << " ";
    }
    cout << endl;
    if (!isStable(a, b, n))
    {
        cout << "Not stable" << endl;
    }
    else
    {
        cout << "Stable" << endl;
    }
    return 0;
}
