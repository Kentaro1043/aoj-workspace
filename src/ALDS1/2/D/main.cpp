#include <bits/stdc++.h>
typedef long long ll;
#define rep(i, n) for (int i = 0; i < (n); i++)
using namespace std;
long long m;
long long cnt;
vector<ll> g;
/**
 * @brief シェルソート用挿入ソート
 * 
 * @param a ソートする配列
 * @param n 配列のサイズ
 * @details O(n^2)、安定なソート
 */
void insertionSortForShellSort(long long a[], long long n, long long g)
{
    for (int i = g; i <= n - 1; i++)
    {
        long long v = a[i];
        long long j = i - g;
        while(j >= 0 && a[j] > v)
        {
            a[j+g] = a[j];
            j-=g;
            cnt++;
        }
        a[j+g] = v;
    }
}
/**
 * @brief シェルソート
 * 
 * @param a ソートする配列
 * @param n 配列のサイズ
 * @details O(n^1.25)が予測される
 */
#include <vector>
void shellSort(long long a[], long long n)
{
    for (int h = 1;;)
    {
        if (h > n) break;
        g.push_back(h);
        h = 3*h+1;
    }
    for (int i = g.size()-1; i >= 0; i--)
    {
        insertionSortForShellSort(a, n, g[i]);
    }
}
int main()
{
    int n; scanf("%d", &n);
    ll a[n];
    rep(i, n) scanf("%lld", &a[i]);
    cnt = 0;
    shellSort(a, n);
    m = g.size();
    cout << m << endl;
    for (int i = m-1; i >= 0; i--)
    {
        printf("%lld", g[i]);
        if (i!=0) printf(" ");
    }
    printf("\n%lld\n", cnt);
    rep(i, n)
    {
        printf("%lld\n", a[i]);
    }
    return 0;
}
