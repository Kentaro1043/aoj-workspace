#include <bits/stdc++.h>
#define rep(i, n) for (int i = 0; i < (int)(n); i++)
typedef long long ll;
using namespace std;
struct p
{
    string name;
    int time;
};
/**
 * @brief キュー(long long専用)
 * 
 * @details 最大の要素の個数100000個
 */
class queue_lld
{
public:
    /**
     * @brief コンストラクタ
     */
    queue_lld()
    {
        head = 0;
        tail = 0;
    }
    /**
     * @brief キューに挿入
     * 
     * @param x 挿入する値
     */
    void enqueue(p x)
    {
        if (isFull())
        {
            return;
        }
        q[tail] = x;
        if (tail+1 == 100003)
        {
            tail = 0;
        }
        else
        {
            tail++;
        }
    }
    /**
     * @brief 先頭の要素を削除
     * 
     * @return long long 削除した値 
     */
    p dequeue()
    {
        p x = q[head];
        if (head + 1 == 100003)
        {
            head = 0;
        }
        else
        {
            head++;
        }
        return x;
    }
    /**
     * @brief 空かどうかの確認
     * 
     * @return true 空
     * @return false 空でない
     */
    bool isEmpty()
    {
        return head == tail;
    }
    /**
     * @brief 満杯かどうかの確認
     * 
     * @return true 満杯
     * @return false 満杯でない
     */
    bool isFull()
    {
        return head == (tail + 1) % 100003;
    }
private:
    /**
     * @brief 本体の配列
     */
    p q[100003];
    /**
     * @brief 先頭の要素のインデックス
     */
    long long head;
    /**
     * @brief 末尾の要素のインデックス
     */
    long long tail;
};
int main()
{
    int n, t; scanf("%d%d", &n, &t);
    queue_lld q;
    rep(i, n)
    {
        string t1; int t2;
        cin >> t1 >> t2;
        p t3; t3.name = t1; t3.time = t2;
        q.enqueue(t3);
    }
    int now = 0;
    while (true)
    {
        int r = t;
        if (q.isEmpty()) break;
        p t1 = q.dequeue();
        if (t1.time <= r)
        {
            r -= t1.time;
            now += t1.time;
            cout << t1.name << " " << now << endl;
        }
        else
        {
            now += r;
            t1.time -= r;
            r = 0;
            q.enqueue(t1);
        }
    }
    return 0;
}
