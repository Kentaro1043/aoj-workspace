#include <bits/stdc++.h>
#define rep(i, n) for (int i = 0; i < (int)(n); i++)
typedef long long ll;
using namespace std;
/**
 * @brief ハッシュテーブル
 */
class hashTable_lld
{
private:
    /**
     * @brief ハッシュ関数用定数
     */
    static const long long m = 1046527;
    /**
     * @brief 本体の配列
     */
    long long h[m];
public:
    /**
     * @brief コンストラクタ
     */
    hashTable_lld()
    {
        for (int i = 0; i < m; i++)
        {
            h[i] = -1;
        }
    }
    /**
     * @brief ハッシュ関数1
     * 
     * @param key 入力値
     * @return long long ハッシュ値 
     */
    long long h1(long long key)
    {
        return key%m;
    }
    /**
     * @brief ハッシュ関数2
     * 
     * @param key 入力値
     * @return long long ハッシュ値 
     */
    long long h2(long long key)
    {
        return 1 + (key % (m - 1));
    }
    /**
     * @brief 値を挿入
     * 
     * @param n 挿入する値
     * @return int 成功したら0、既に値があったら1
     */
    int insert(long long n)
    {
        long long key;
        for (int i = 0; ; i++)
        {
            key = (h1(n) + i * h2(n)) % m;
            if (h[key] == n)
            {
                return 1;
            }
            else if (h[key] == -1)
            {
                h[key] = n;
                return 0;
            }
        }
        return 0;
    }
    /**
     * @brief 値を検索
     * 
     * @param n 検索する値
     * @return true 存在する
     * @return false 存在しない
     */
    bool search(long long n)
    {
        long long key;
        for (int i = 0; ; i++)
        {
            key = (h1(n) + i * h2(n)) % m;
            if (h[key] == n)
            {
                return true;
            }
            else if (h[key] == -1)
            {
                return false;
            }
        }
        return false;
    }
};
int main()
{
    int n; cin >> n;
    hashTable_lld h;
    rep(i, n)
    {
        string s, t; cin >> s >> t;
        ll k = 0;
        rep(j, t.size())
        {
            ll t2;
            if (t[j]=='A') {t2 = 1;}
            else if (t[j]=='C') {t2 = 2;}
            else if (t[j]=='G') {t2 = 3;}
            else {t2 = 4;}
            k += t2*pow(10, j);
        }
        if (s == "insert")
        {
            h.insert(k);
        }
        else
        {
            cout << (h.search(k)?"yes":"no") << endl;
        }
    }
    return 0;
}
